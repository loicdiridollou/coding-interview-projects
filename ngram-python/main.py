"""Code to compute n-gram log-probability."""
import math


class Ngram():
    """N-gram class with all necessary methods."""
    def __init__(self, data, n) -> None:
        """Initialization of the class.

        Here we store the length, the data and generate the n-grams and
        n-1-grams upon initialization to save computation time in the future.
        We don't address the case of punctuations and capitalized letters in
        this implementation. Some edge cases of empty data are not addressed
        since it is not the focus of the exercise.
        """
        self.data = data
        self.n = n
        self.generate_n_gram()

    def generate_n_gram(self):
        """Generate the n-grams and n-1-grams and counting them."""
        res_shrt = []
        res_long = []
        for sentence in self.data:
            new_stn = self.add_pseudo_words(sentence, self.n).split(' ')
            for i in range(len(new_stn)-self.n + 2):
                res_shrt.append(" ".join(new_stn[i:i + self.n - 1]))
            for i in range(len(new_stn)-self.n + 1):
                res_long.append(" ".join(new_stn[i:i + self.n]))
        # count the occurence and store in a dictionary for faster computation
        self.counter_shrt = self.counter(res_shrt)
        self.counter_long = self.counter(res_long)

    @staticmethod
    def add_pseudo_words(sentence, num):
        """Augment the sentence."""
        return "<s> " * (num - 1) + sentence + " </s>" * (num - 1)

    @staticmethod
    def counter(lst):
        """Reimplementation of collections.Counter."""
        dic = {}
        for element in lst:
            dic[element] = dic.get(element, 0) + 1
        return dic

    def calculate_prob(self, word, previous):
        """We compute the fraction of frequency of the n and n-1-gram.

        This is O(1) operation so no need to cache the results for reuse.
        """
        val = (self.counter_long.get(previous + " " + word, 0)
               / self.counter_shrt.get(previous, 1))
        return val

    def log_prob(self, sentence):
        """We apply Markov chain rule on the sentence."""
        if not sentence:
            raise ValueError("Please pass a well formed sentence.")
        split_sentence = self.add_pseudo_words(sentence, self.n).split(' ')
        res = 1
        for i, word in enumerate(split_sentence):
            if i < self.n - 1:
                # We skip the first pseudo-words as indicated
                continue
            res *= self.calculate_prob(word,
                                       ' '.join(split_sentence[i+1-self.n:i]))
        return math.log(res)


def main():
    """Main."""
    data = [
        "I am Sam",
        "Sam I am",
        "Sam likes green eggs and ham",
        "I like green eggs and pizza",
        "I do not like green eggs and ham",
        "I do not eat green eggs and ham",
        ]
    ngram = Ngram(data, 3)
    # should print the log (natural log) probability of the sentence
    return ngram.log_prob("I do not like green eggs and ham")


if __name__ == "__main__":
    print(main())
